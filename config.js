module.exports = {
    'name'  : 'Blog 17',
    'camel' : 'Blog17',
    'slug'  : 'blog-17',
    'dob'   : 'Blog_17_1440',
    'desc'  : 'Blog posts in varying width columns as full images overlayed with title and post meta.',
}