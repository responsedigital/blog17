<!-- Start Blog 17 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Blog posts in varying width columns as full images overlayed with title and post meta. -->
@endif
<div class="blog-17"  is="fir-blog-17">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="blog-17__wrap">
    @for($x = 0; $x < 3; $x++)
      <a class="blog-17__post" href="#">
        <img class="blog-17__post-img" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="Placeholder">
        <div class="blog-17__post-overlay">
          <p class="blog-17__post-date">{{ $post['date'] ?: $faker->dayOfMonth($max = 'now') . ' ' . $faker->monthName($max = 'now')  . ' ' . $faker->year($max = 'now') }}</p>
          <h4 class="blog-17__post-title">{{ $post['title'] ?: $faker->text($maxNbChars = 20) }}</h4>
        </div>
      </a>
    @endfor
  </div>
</div>
<!-- End Blog 17 -->
