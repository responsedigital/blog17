class Blog17 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initBlog17()
    }

    initBlog17 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Blog 17")
    }

}

window.customElements.define('fir-blog-17', Blog17, { extends: 'div' })
